package yim.david.apimockapp.presentation.ui.main.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import yim.david.apimockapp.domain.models.Forecast
import yim.david.apimockapp.domain.usecases.FetchForecastForDayUseCase

class DetailsFragmentViewModel(private val fetchForecastForDayUseCase: FetchForecastForDayUseCase)
    : ViewModel() {

    private val forecastMutableLiveData = MutableLiveData<Forecast>()
    private val errorMutableLiveData = MutableLiveData<Void>()


    override fun onCleared() {
        fetchForecastForDayUseCase.unsubscribe()
        super.onCleared()
    }


    fun fetchForecastByDay(forceRefresh: Boolean, day: Int) {
        fetchForecastForDayUseCase.subscribe(FetchForecastForDayUseCase.Params.forDayWithForceRefresh(forceRefresh, day),
            {
                forecastMutableLiveData.postValue(it)
            },
            {
                errorMutableLiveData.postValue(null)
            }
        )
    }


    fun getForecastLiveData(): LiveData<Forecast> = forecastMutableLiveData
    fun getErrorLiveData(): LiveData<Void> = errorMutableLiveData

}