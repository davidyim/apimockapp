package yim.david.apimockapp.presentation.ui.main.details

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import yim.david.apimockapp.domain.models.Forecast
import yim.david.apimockapp.presentation.extensions.formatSecondsToHourMinute
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.signature.ObjectKey
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import yim.david.apimockapp.R
import yim.david.apimockapp.databinding.FragmentDetailsBinding
import java.text.NumberFormat

class DetailsFragment : ScopeFragment() {

    private val detailsFragmentViewModel: DetailsFragmentViewModel by viewModel()
    private var binding: FragmentDetailsBinding? = null
    private val arguments by navArgs<DetailsFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBackNavigation()
        observeForecastItem()
        observeError()
    }

    override fun onStart() {
        super.onStart()
        detailsFragmentViewModel.fetchForecastByDay(false, arguments.day)
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }


    private fun setupBackNavigation() {
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback)
    }

    private fun observeForecastItem() {
        detailsFragmentViewModel.getForecastLiveData().observe(viewLifecycleOwner) { forecast ->
            binding?.apply {
                description.text = forecast.description
                temperature.text = getString(R.string.fragment_details_temperature_format, forecast.lowestTemperature.toString(), forecast.highestTemperature.toString())
                chanceOfRain.text = getString(R.string.fragment_details_chance_of_rain, NumberFormat.getPercentInstance().format(forecast.chanceOfRain))
                sunrise.text = getString(R.string.fragment_details_sunrise, forecast.sunrise.formatSecondsToHourMinute())
                sunset.text = getString(R.string.fragment_details_sunset, forecast.sunset.formatSecondsToHourMinute())

                loadImage(forecast, true)
                downloadButton.setOnClickListener {
                    downloadButton.isEnabled = false
                    imageProgressBar.visibility = View.VISIBLE
                    loadImage(forecast, false)
                }
            }
        }
    }

    private fun observeError() {
        detailsFragmentViewModel.getErrorLiveData().observe(viewLifecycleOwner) {
            // Error means argument day was wrong
            navigateUp()
        }
    }

    private fun navigateUp() {
        findNavController().navigateUp()
    }

    private fun loadImage(forecast: Forecast, onlyFromCache: Boolean) {
        binding?.apply {
            Glide.with(requireContext())
                .load(forecast.imageUrl)
                .onlyRetrieveFromCache(onlyFromCache)
                // Since imageUrl is always the same url giving random image, use day as signature
                .signature(ObjectKey(forecast.day))
                .listener(
                    object: RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            downloadButton.isEnabled = true
                            imageProgressBar.visibility = View.GONE
                            downloadButton.visibility = View.VISIBLE
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            imageProgressBar.visibility = View.GONE
                            downloadButton.visibility = View.GONE
                            return false
                        }
                    }
                )
                .into(image)
        }
    }

}