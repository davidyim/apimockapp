package yim.david.apimockapp.presentation.ui.main.master.list

enum class ForecastListFragmentState {
    LOADING,
    LOADED,
    ERROR
}