package yim.david.apimockapp.presentation.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import yim.david.apimockapp.R
import yim.david.apimockapp.presentation.ui.main.details.DetailsFragmentArgs

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                navController.navigateUp()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun setupActionBar() {
        // Use navigation UI to setup ForecastFragment title
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupActionBarWithNavController(this, navController)

        // Since DetailsFragment title needs an argument, use navController
        navController.addOnDestinationChangedListener { _, destination, arguments ->
            supportActionBar?.title = when (destination.id) {
                R.id.details_fragment -> getString(R.string.fragment_details_title,
                    arguments?.let { DetailsFragmentArgs.fromBundle(arguments).day.toString() })
                else -> getString(R.string.fragment_master_title)
            }
        }
    }
}
