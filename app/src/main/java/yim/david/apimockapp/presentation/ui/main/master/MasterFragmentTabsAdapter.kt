package yim.david.apimockapp.presentation.ui.main.master

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import yim.david.apimockapp.presentation.ui.main.master.list.ForecastListFragment

class MasterFragmentTabsAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = TabType.values().size

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            TabType.UPCOMING.ordinal -> ForecastListFragment.newUpcomingForecastFragment()
            TabType.HOTTEST.ordinal -> ForecastListFragment.newHottestForecastFragment()
            else -> throw RuntimeException()
        }
    }
}