package yim.david.apimockapp.presentation.ui

import android.app.Application
import yim.david.apimockapp.data.di.dataModule
import yim.david.apimockapp.data.di.networkModule
import yim.david.apimockapp.domain.di.domainModule
import yim.david.apimockapp.presentation.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ApiMockApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@ApiMockApplication)
            modules(listOf(
                networkModule,
                dataModule,
                domainModule,
                presentationModule
            ))
        }
    }
}