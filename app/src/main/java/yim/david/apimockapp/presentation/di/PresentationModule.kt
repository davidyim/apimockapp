package yim.david.apimockapp.presentation.di

import yim.david.apimockapp.presentation.ui.main.details.DetailsFragment
import yim.david.apimockapp.presentation.ui.main.details.DetailsFragmentViewModel
import yim.david.apimockapp.presentation.ui.main.master.list.ForecastListAdapter
import yim.david.apimockapp.presentation.ui.main.master.list.ForecastListFragment
import yim.david.apimockapp.presentation.ui.main.master.list.ForecastListFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    scope<ForecastListFragment> {
        scoped { ForecastListAdapter() }
        viewModel { ForecastListFragmentViewModel(get(), get()) }
    }

    scope<DetailsFragment> {
        viewModel { DetailsFragmentViewModel(get()) }
    }

}