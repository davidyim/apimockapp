package yim.david.apimockapp.presentation.ui.main.master

enum class TabType {
    UPCOMING,
    HOTTEST
}