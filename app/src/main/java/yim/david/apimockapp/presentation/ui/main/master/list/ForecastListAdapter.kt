package yim.david.apimockapp.presentation.ui.main.master.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import yim.david.apimockapp.databinding.ItemForecastBinding
import yim.david.apimockapp.domain.models.Forecast

class ForecastListAdapter: RecyclerView.Adapter<ForecastViewHolder>() {

    private val forecastList = mutableListOf<Forecast>()
    private var onForecastClickListener: (Int) -> Unit = {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        return ForecastViewHolder(ItemForecastBinding
            .inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) =
        holder.bind(forecastList[position], onForecastClickListener)

    override fun getItemCount(): Int = forecastList.size


    fun setForecastList(forecastList: List<Forecast>) {
        this.forecastList.clear()
        this.forecastList.addAll(forecastList)
        notifyDataSetChanged()
    }

    fun setOnForecastClickListener(onForecastClickListener: (Int) -> Unit) {
        this.onForecastClickListener = onForecastClickListener
    }
}