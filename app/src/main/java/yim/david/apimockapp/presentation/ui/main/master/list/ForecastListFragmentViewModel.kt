package yim.david.apimockapp.presentation.ui.main.master.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import yim.david.apimockapp.domain.models.Forecast
import yim.david.apimockapp.domain.usecases.FetchHottestForecastListUseCase
import yim.david.apimockapp.domain.usecases.FetchUpcomingForecastListUseCase

class ForecastListFragmentViewModel(
    private val fetchUpcomingForecastListUseCase: FetchUpcomingForecastListUseCase,
    private val fetchHottestForecastListUseCase: FetchHottestForecastListUseCase
) : ViewModel() {

    private val viewStateMutableLiveData = MutableLiveData<ForecastListFragmentState>()
    private val forecastMutableLiveData = MutableLiveData<List<Forecast>>()


    override fun onCleared() {
        fetchUpcomingForecastListUseCase.unsubscribe()
        fetchHottestForecastListUseCase.unsubscribe()
        super.onCleared()
    }


    fun fetchUpcomingForecast(forceRefresh: Boolean) {
        viewStateMutableLiveData.postValue(ForecastListFragmentState.LOADING)
        fetchUpcomingForecastListUseCase.subscribe(FetchUpcomingForecastListUseCase.Params.withForceRefresh(forceRefresh),
            {
                forecastMutableLiveData.postValue(it)
                viewStateMutableLiveData.postValue(ForecastListFragmentState.LOADED)
            },
            {
                viewStateMutableLiveData.postValue(ForecastListFragmentState.ERROR)
            }
        )
    }

    fun fetchHottestForecast(forceRefresh: Boolean) {
        viewStateMutableLiveData.postValue(ForecastListFragmentState.LOADING)
        fetchHottestForecastListUseCase.subscribe(FetchHottestForecastListUseCase.Params.withForceRefresh(forceRefresh),
            {
                forecastMutableLiveData.postValue(it)
                viewStateMutableLiveData.postValue(ForecastListFragmentState.LOADED)
            },
            {
                viewStateMutableLiveData.postValue(ForecastListFragmentState.ERROR)
            }
        )
    }

    fun getViewStateLiveData(): LiveData<ForecastListFragmentState> = viewStateMutableLiveData
    fun getForecastLiveData(): LiveData<List<Forecast>> = forecastMutableLiveData

}