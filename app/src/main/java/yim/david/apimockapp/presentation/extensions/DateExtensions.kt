package yim.david.apimockapp.presentation.extensions

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

private const val HOUR_MINUTE_FORMAT = "HH:mm"

fun Long.formatSecondsToHourMinute(): String =
    SimpleDateFormat(HOUR_MINUTE_FORMAT, Locale.getDefault()).format(Date(TimeUnit.SECONDS.toMillis(this)))
