package yim.david.apimockapp.presentation.ui.main.master

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import yim.david.apimockapp.R
import yim.david.apimockapp.databinding.FragmentMasterBinding

class MasterFragment : Fragment() {

    private var binding: FragmentMasterBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMasterBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }


    private fun setupViews() {
        binding?.apply {
            viewPager.adapter = MasterFragmentTabsAdapter(this@MasterFragment)

            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = when (position) {
                    TabType.UPCOMING.ordinal -> getString(R.string.fragment_master_tab_upcoming)
                    TabType.HOTTEST.ordinal -> getString(R.string.fragment_master_tab_hottest)
                    else -> null
                }
            }.attach()
        }
    }

}