package yim.david.apimockapp.presentation.ui.main.master.list

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.signature.ObjectKey
import yim.david.apimockapp.R
import yim.david.apimockapp.databinding.ItemForecastBinding
import yim.david.apimockapp.domain.models.Forecast

private const val THUMBNAIL_SIZE_MULTIPLIER = 0.1f

class ForecastViewHolder(private val binding: ItemForecastBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(forecast: Forecast, onItemClicked: (Int) -> Unit) {
        with(binding) {
            val context = root.context
            description.text = context.getString(R.string.item_forecast_text_format, forecast.day.toString(), forecast.description)

            Glide.with(context)
                .load(forecast.imageUrl)
                .onlyRetrieveFromCache(true)
                // Since imageUrl is always the same url giving random image, use day as signature
                .signature(ObjectKey(forecast.day))
                .thumbnail(THUMBNAIL_SIZE_MULTIPLIER)
                // Use a transparent placeholder to resize imageView
                .placeholder(ColorDrawable(Color.TRANSPARENT))
                .centerCrop()
                .into(image)

            root.setOnClickListener {
                onItemClicked(forecast.day)
            }
        }
    }

}