package yim.david.apimockapp.presentation.ui.main.master.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import yim.david.apimockapp.presentation.ui.main.master.MasterFragmentDirections
import yim.david.apimockapp.presentation.ui.main.master.TabType
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import yim.david.apimockapp.databinding.FragmentForecastListBinding

private const val TAB_TYPE_ARGUMENT = "tabType"

class ForecastListFragment : ScopeFragment() {

    companion object {

        fun newUpcomingForecastFragment(): ForecastListFragment {
            return ForecastListFragment().apply {
                arguments = Bundle().apply {
                    putString(TAB_TYPE_ARGUMENT, TabType.UPCOMING.name)
                }
            }
        }

        fun newHottestForecastFragment(): ForecastListFragment {
            return ForecastListFragment().apply {
                arguments = Bundle().apply {
                    putString(TAB_TYPE_ARGUMENT, TabType.HOTTEST.name)
                }
            }
        }
    }

    private val forecastListAdapter: ForecastListAdapter by inject()
    private val forecastListFragmentViewModel: ForecastListFragmentViewModel by viewModel()
    private var binding: FragmentForecastListBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentForecastListBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        observeViewState()
        observeForecastList()
    }

    override fun onStart() {
        super.onStart()
        when (arguments?.getString(TAB_TYPE_ARGUMENT)) {
            TabType.UPCOMING.name -> forecastListFragmentViewModel.fetchUpcomingForecast(false)
            TabType.HOTTEST.name -> forecastListFragmentViewModel.fetchHottestForecast(false)
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }


    private fun setupViews() {
        binding?.apply {
            recyclerView.apply {
                setHasFixedSize(true)
                val linearLayoutManager = LinearLayoutManager(requireContext())
                layoutManager = linearLayoutManager
                addItemDecoration(DividerItemDecoration(requireContext(), linearLayoutManager.orientation))
                forecastListAdapter.setOnForecastClickListener { day ->
                    findNavController().navigate(
                        MasterFragmentDirections.actionMasterFragmentToDetailsFragment(day)
                    )
                }
                adapter = forecastListAdapter
            }

            retryButton.setOnClickListener {
                when (arguments?.getString(TAB_TYPE_ARGUMENT)) {
                    TabType.UPCOMING.name -> forecastListFragmentViewModel.fetchUpcomingForecast(true)
                    TabType.HOTTEST.name -> forecastListFragmentViewModel.fetchHottestForecast(true)
                }
            }
        }
    }

    private fun observeViewState() {
        forecastListFragmentViewModel.getViewStateLiveData().observe(viewLifecycleOwner) {
            binding?.apply {
                when (it) {
                    ForecastListFragmentState.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                        errorText.visibility = View.GONE
                        retryButton.visibility = View.GONE
                    }
                    ForecastListFragmentState.LOADED -> {
                        progressBar.visibility = View.GONE
                        recyclerView.visibility = View.VISIBLE
                        errorText.visibility = View.GONE
                        retryButton.visibility = View.GONE
                    }
                    else -> {
                        progressBar.visibility = View.GONE
                        recyclerView.visibility = View.GONE
                        errorText.visibility = View.VISIBLE
                        retryButton.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun observeForecastList() {
        forecastListFragmentViewModel.getForecastLiveData().observe(viewLifecycleOwner) {
            forecastListAdapter.setForecastList(it)
        }
    }
}