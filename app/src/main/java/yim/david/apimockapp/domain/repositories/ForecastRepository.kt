package yim.david.apimockapp.domain.repositories

import yim.david.apimockapp.domain.models.Forecast
import io.reactivex.Single

interface ForecastRepository {

    fun fetchUpcomingForecast(forceRefresh: Boolean): Single<List<Forecast>>
    fun fetchHottestForecast(forceRefresh: Boolean): Single<List<Forecast>>
    fun fetchForecastForDay(forceRefresh: Boolean, day: Int): Single<Forecast>
}