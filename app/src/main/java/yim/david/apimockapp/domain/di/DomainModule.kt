package yim.david.apimockapp.domain.di

import yim.david.apimockapp.domain.executor.ThreadExecutor
import yim.david.apimockapp.domain.usecases.FetchForecastForDayUseCase
import yim.david.apimockapp.domain.usecases.FetchHottestForecastListUseCase
import yim.david.apimockapp.domain.usecases.FetchUpcomingForecastListUseCase
import org.koin.dsl.module

val domainModule = module {
    single { ThreadExecutor() }

    single { FetchUpcomingForecastListUseCase(get(), get()) }
    single { FetchHottestForecastListUseCase(get(), get()) }
    single { FetchForecastForDayUseCase(get(), get()) }
}