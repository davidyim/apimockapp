package yim.david.apimockapp.domain.usecases

import yim.david.apimockapp.domain.executor.ThreadExecutor
import yim.david.apimockapp.domain.models.Forecast
import yim.david.apimockapp.domain.repositories.ForecastRepository
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FetchHottestForecastListUseCase(
    private val threadExecutor: ThreadExecutor,
    private val forecastRepository: ForecastRepository) {

    class Params private constructor(val forceRefresh: Boolean) {

        companion object {
            @JvmStatic
            fun withForceRefresh(forceRefresh: Boolean): Params = Params(forceRefresh)
        }
    }


    private var disposable: Disposable? = null


    fun subscribe(params: Params, onSuccess: ((List<Forecast>) -> Unit), onError: ((Throwable) -> Unit)) {
        disposable = forecastRepository.fetchHottestForecast(params.forceRefresh)
            .subscribeOn(Schedulers.from(threadExecutor))
            .subscribe(onSuccess, onError)
    }

    fun unsubscribe() {
        if (disposable?.isDisposed == false) {
            disposable?.dispose()
        }
    }

}