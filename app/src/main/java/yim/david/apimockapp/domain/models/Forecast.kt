package yim.david.apimockapp.domain.models

data class Forecast(
    val day: Int,
    val description: String,
    val sunrise: Long,
    val sunset: Long,
    val chanceOfRain: Double,
    val highestTemperature: Int,
    val lowestTemperature: Int,
    val imageUrl: String
)