package yim.david.apimockapp.data.managers.cache

import yim.david.apimockapp.domain.models.Forecast

class CacheManagerImpl: CacheManager {

    private var forecastList : List<Forecast>? = null

    override fun saveForecastList(forecastList: List<Forecast>) {
        this.forecastList = forecastList
    }

    override fun getForecastList(): List<Forecast>? = forecastList


}