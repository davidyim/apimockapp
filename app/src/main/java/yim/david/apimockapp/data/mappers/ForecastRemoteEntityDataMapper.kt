package yim.david.apimockapp.data.mappers

import yim.david.apimockapp.data.entities.ForecastRemoteEntity
import yim.david.apimockapp.domain.models.Forecast


class ForecastRemoteEntityDataMapper {

    fun transformRemoteToModel(forecastRemoteEntity: ForecastRemoteEntity): Forecast =
        Forecast(
            forecastRemoteEntity.day.toInt(),
            forecastRemoteEntity.description,
            forecastRemoteEntity.sunrise,
            forecastRemoteEntity.sunset,
            forecastRemoteEntity.chanceOfRain,
            forecastRemoteEntity.highestTemperature,
            forecastRemoteEntity.lowestTemperature,
            forecastRemoteEntity.imageUrl
        )

    fun transformRemoteToModelList(forecastRemoteEntityList: List<ForecastRemoteEntity>): List<Forecast> =
        forecastRemoteEntityList.map { transformRemoteToModel(it) }
}