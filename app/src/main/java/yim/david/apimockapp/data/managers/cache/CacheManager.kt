package yim.david.apimockapp.data.managers.cache

import yim.david.apimockapp.domain.models.Forecast

interface CacheManager {

    fun saveForecastList(forecastList: List<Forecast>)

    fun getForecastList(): List<Forecast>?
}