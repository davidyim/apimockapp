package yim.david.apimockapp.data.services

import yim.david.apimockapp.data.entities.ForecastRemoteEntity
import io.reactivex.Single
import retrofit2.http.GET

interface ForecastService {

    @GET("forecast")
    fun fetchForecast(): Single<List<ForecastRemoteEntity>>

}