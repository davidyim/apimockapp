package yim.david.apimockapp.data.managers.api

import yim.david.apimockapp.data.entities.ForecastRemoteEntity
import yim.david.apimockapp.data.services.ForecastService
import io.reactivex.Single

class ApiManagerImpl(private val forecastService: ForecastService) : ApiManager {

    override fun fetchForecast(): Single<List<ForecastRemoteEntity>> =
        forecastService.fetchForecast()

}