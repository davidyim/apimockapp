package yim.david.apimockapp.data.di

import yim.david.apimockapp.data.services.ForecastService
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import yim.david.apimockapp.BuildConfig

val networkModule = module {
    single { httpLoggingInterceptor() }
    single { okHttpClient(get()) }
    single { retrofit(get()) }
    single { forecastService(get()) }
}


private fun forecastService(retrofit: Retrofit) = retrofit.create(ForecastService::class.java)

private fun retrofit(okHttpClient: OkHttpClient) = Retrofit.Builder()
    .baseUrl(BuildConfig.BASE_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .build()

private fun okHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
    .addInterceptor(httpLoggingInterceptor)
    .build()

private fun httpLoggingInterceptor() = HttpLoggingInterceptor().apply {
    if (BuildConfig.DEBUG) {
        level = HttpLoggingInterceptor.Level.BODY
    }
}