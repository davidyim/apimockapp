package yim.david.apimockapp.data.managers.api

import yim.david.apimockapp.data.entities.ForecastRemoteEntity
import io.reactivex.Single

interface ApiManager {

    fun fetchForecast(): Single<List<ForecastRemoteEntity>>

}