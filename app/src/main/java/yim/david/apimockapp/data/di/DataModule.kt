package yim.david.apimockapp.data.di

import yim.david.apimockapp.data.managers.api.ApiManager
import yim.david.apimockapp.data.managers.api.ApiManagerImpl
import yim.david.apimockapp.data.managers.cache.CacheManager
import yim.david.apimockapp.data.managers.cache.CacheManagerImpl
import yim.david.apimockapp.data.mappers.ForecastRemoteEntityDataMapper
import yim.david.apimockapp.data.repositories.ForecastRepositoryImpl
import yim.david.apimockapp.data.services.ForecastService
import yim.david.apimockapp.domain.repositories.ForecastRepository
import org.koin.dsl.module

val dataModule = module {
    single { forecastRepository(get(), get(), get()) }

    single { apiManager(get()) }
    single { cacheManager() }

    single { forecastRemoteEntityDataMapper() }
}


private fun forecastRepository(apiManager: ApiManager,
                               cacheManager: CacheManager,
                               forecastRemoteEntityDataMapper: ForecastRemoteEntityDataMapper)
: ForecastRepository = ForecastRepositoryImpl(apiManager, cacheManager, forecastRemoteEntityDataMapper)

private fun apiManager(forecastService: ForecastService): ApiManager =
    ApiManagerImpl(forecastService)

private fun cacheManager(): CacheManager = CacheManagerImpl()

private fun forecastRemoteEntityDataMapper() = ForecastRemoteEntityDataMapper()
