package yim.david.apimockapp.data.repositories

import yim.david.apimockapp.data.managers.api.ApiManager
import yim.david.apimockapp.data.managers.cache.CacheManager
import yim.david.apimockapp.data.mappers.ForecastRemoteEntityDataMapper
import yim.david.apimockapp.domain.models.Forecast
import yim.david.apimockapp.domain.repositories.ForecastRepository
import io.reactivex.Single

private const val HOTTEST_CHANCE_OF_RAIN_THRESHOLD = 0.5

class ForecastRepositoryImpl(
    private val apiManager: ApiManager,
    private val cacheManager: CacheManager,
    private val forecastRemoteEntityDataMapper: ForecastRemoteEntityDataMapper
) : ForecastRepository {

    override fun fetchUpcomingForecast(forceRefresh: Boolean): Single<List<Forecast>> {
        return fetchForecast(forceRefresh).map { forecastList ->
            forecastList.sortedBy { forecast ->
                forecast.day
            }
        }
    }

    override fun fetchHottestForecast(forceRefresh: Boolean): Single<List<Forecast>> {
        return fetchForecast(forceRefresh).map { forecastList ->
            forecastList.filter { forecast ->
                forecast.chanceOfRain < HOTTEST_CHANCE_OF_RAIN_THRESHOLD
            }.sortedByDescending { forecast ->
                forecast.highestTemperature
            }
        }
    }

    override fun fetchForecastForDay(forceRefresh: Boolean, day: Int): Single<Forecast> {
        return fetchForecast(forceRefresh).map { forecastList ->
            forecastList.find {
                it.day == day
            }
        }
    }


    private fun fetchForecast(forceRefresh: Boolean) : Single<List<Forecast>> {
        val cachedForecast = if (forceRefresh) {
            null
        } else {
            cacheManager.getForecastList()
        }

        return if (cachedForecast != null) {
            Single.just(cachedForecast)
        } else {
            apiManager.fetchForecast().map {
                val forecastList = forecastRemoteEntityDataMapper.transformRemoteToModelList(it)
                cacheManager.saveForecastList(forecastList)
                forecastList
            }
        }
    }

}