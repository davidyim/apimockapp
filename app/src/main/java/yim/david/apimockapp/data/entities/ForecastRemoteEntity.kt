package yim.david.apimockapp.data.entities

import com.google.gson.annotations.SerializedName

data class ForecastRemoteEntity(
    @SerializedName("day")
    val day: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("sunrise")
    val sunrise: Long,

    @SerializedName("sunset")
    val sunset: Long,

    @SerializedName("chance_rain")
    val chanceOfRain: Double,

    @SerializedName("high")
    val highestTemperature: Int,

    @SerializedName("low")
    val lowestTemperature: Int,

    @SerializedName("image")
    val imageUrl: String
)