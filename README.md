## Overview

This is an application used to test new code architecture, good practices and libraries. It uses a mocked weather API and follows the following functional requirements.

## Functional Requirements

The app is to fetch weather data from a RESTful mocked API and display the information in master (list) and detail screen. The app will cache data in case the API is unavailable.

### Master Screen

The master screen will show "Forecast" as the title in the toolbar. Below the toolbar should be a pair of tabs with "Upcoming" and "Hottest" tabs. Below the tabs should be a list displaying the forecast in the following order:
- Upcoming: All weather data in order of day.
- Hottest: Only days with less than 50% chance of rain, ordered from hottest to coldest.

The content of the rows should be in format "Day \<day\>: \<description\>" where day and description are returned by the API. If the image for a particular day is downloaded (initiated by user action), then the row should also display a thumbnail of the image.

### Detail Screen

The toolbar should display "Day \<day\>". The screen will display the weather data arranged as you see fit. Below the test data, the image should be displayed if downloaded. Aligned to the bottom of the screen, a "Download Image" button should be displayed to download the image and disappear on succesful download.

## Build

Project can be built using ```./gradlew clean assembleDevDebug```

## Technical stack

- 100% Kotlin code with AndroidX
- MVVM Clean architecture
- Dependency Injection using Koin
- ViewBinding, Navigation